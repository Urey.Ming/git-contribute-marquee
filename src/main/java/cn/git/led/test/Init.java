package cn.git.led.test;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.util.Calendar;

/**
 * 运行方法
 *
 * @author fuce
 * @ClassName: Init
 * @date 2019-12-02 00:28
 * 码云颜色对应标识
 * less == 0     0
 * little < 8    1
 * some < 15     8
 * many <= 23    16
 * much > 23     24
 */
public class Init {

    public static void main(String[] args) throws Exception {


        //码云LED开始时间
        DateTime start_date = DateUtil.parseDate("2018-12-03");
        String start_time = "21:00:00";

        //获取二维码

        int[][] Led = {
                {0, 0, 0, 0, 24, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, 24, 24, 0, 0, 0, 0, 24, 24, 24, 24, 24, 24, 24, 0, 0, 0, 24, 24, 24, 24, 24, 24, 0, 0, 0, 24, 0, 0, 24, 0, 0, 0, 0, 0},
                {0, 0, 0, 24, 1, 24, 24, 24, 24, 1, 24, 0, 0, 0, 24, 16, 8, 8, 1, 8, 8, 16, 24, 0, 0, 24, 8, 8, 1, 1, 1, 8, 8, 24, 0, 0, 24, 16, 16, 16, 16, 24, 0, 0, 24, 24, 24, 24, 24, 24, 0, 0, 0, 0},
                {0, 0, 0, 24, 1, 1, 1, 1, 1, 1, 24, 0, 0, 0, 24, 8, 8, 1, 1, 1, 8, 8, 24, 0, 24, 16, 8, 8, 1, 1, 1, 8, 8, 16, 24, 0, 24, 1, 16, 16, 1, 24, 0, 0, 24, 1, 16, 16, 1, 24, 0, 0, 0, 0},
                {0, 8, 8, 24, 1, 24, 1, 1, 24, 1, 24, 8, 8, 0, 24, 16, 24, 24, 24, 24, 24, 16, 24, 0, 24, 16, 16, 24, 24, 24, 24, 24, 16, 16, 24, 0, 24, 16, 16, 16, 16, 24, 0, 0, 24, 24, 24, 24, 24, 24, 0, 0, 0, 0},
                {0, 0, 0, 24, 1, 1, 16, 16, 1, 1, 24, 0, 0, 0, 24, 24, 1, 24, 1, 24, 1, 24, 24, 0, 0, 24, 24, 1, 24, 1, 24, 1, 24, 24, 0, 0, 24, 24, 24, 24, 24, 24, 0, 24, 0, 0, 24, 24, 0, 0, 0, 0, 0, 0},
                {0, 8, 8, 24, 1, 1, 1, 1, 1, 1, 24, 8, 8, 0, 0, 24, 1, 1, 1, 1, 1, 24, 0, 0, 0, 0, 24, 1, 1, 1, 1, 1, 24, 0, 0, 0, 0, 0, 24, 24, 0, 0, 0, 0, 24, 24, 24, 24, 24, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 24, 24, 24, 24, 24, 24, 0, 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, 0, 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, 0, 0, 0, 0, 24, 24, 24, 24, 24, 24, 0, 0, 0, 0, 24, 24, 24, 0, 0, 0, 0, 0}
        };


        //设置系统时间
        Boolean settime = WindowsSetSystemTime.setWindosTime(start_date + " " + start_time);
        System.out.println("设置系统时间>" + settime);
        Thread.sleep(500L);
        int daynum = 0;
        for (int i = 0; i < Led[0].length; i++) {

            for (int j = 0; j < Led.length; j++) {

                //提交次数
                int submitNum = Led[j][i];
                int jsq = 0;

                for (int j2 = 0; j2 < submitNum; j2++) {
                    //git提交
                    Boolean boolean1 = GitUtil.commitFiles();
                    //判断还在提交，如果没结束，等待提交
                    do {
                        try {
                            Thread.sleep(500L);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.err.println("等待提交");
                        boolean1 = true;
                    } while (boolean1 == false);
                    jsq++;

                }
                System.out.print(DateUtil.now() + ">>执行第" + jsq + "次");
                //设置时间累加
                daynum++;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(start_date);
                calendar.add(Calendar.DATE, daynum);
                //设置系统时间
                Boolean settime2 = WindowsSetSystemTime.setWindosTime(DateUtil.formatDateTime(calendar.getTime()));
                System.out.println("下一个时间:" + DateUtil.formatDate(calendar.getTime()) + "设置成功" + settime2);
                System.out.println();
            }
        }
        System.out.println(">>完成");
    }


}
